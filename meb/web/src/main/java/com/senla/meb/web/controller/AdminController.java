package com.senla.meb.web.controller;

import com.senla.meb.service.api.IAdminService;
import com.senla.meb.web.dto.MemberRequestStatusDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(AdminController.URL_ADMIN)
public class AdminController extends AbstractController {

    protected static final String URL_ADMIN = "/admin";

    @Autowired
    private IAdminService adminService;

    @PutMapping("/changeRequestStatus")
    public ResponseEntity<?> changeMembershipRequestStatus(@RequestBody MemberRequestStatusDto memberRequestStatusDto) {
        return voidRestResponseHandler.handle(
                () -> adminService.changeMembershipRequestStatus(memberRequestStatusDto.getIdMember(),
                        memberRequestStatusDto.getStatus()));
    }
}