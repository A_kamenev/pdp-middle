package com.senla.meb.web.handler.api;

import org.springframework.http.ResponseEntity;

import java.util.function.Supplier;

public interface IRestResponseHandler extends IGenericResponseHandler<ResponseEntity<?>, Supplier<?>> {


}
