package com.senla.meb.web.controller;

import com.senla.meb.model.Book;
import com.senla.meb.service.api.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/books")
public class BookController extends AbstractController {

    @Autowired
    private IBookService bookService;

    @GetMapping
    public ResponseEntity getAllBooks() {
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity getBookById(@PathVariable Long id) {
        return restResponseHandler.handle(() -> bookService.getBookById(id));
    }

    @PostMapping
    public ResponseEntity addBook(@RequestBody Book book) {
        return voidRestResponseHandler.handle(() -> bookService.addBook(book));
    }

    @PutMapping
    public ResponseEntity updateBook(@RequestBody Book book) {
        return voidRestResponseHandler.handle(() -> bookService.updateBook(book));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteBook(@PathVariable Long id) {
        return voidRestResponseHandler.handle(() -> bookService.deleteBook(id));
    }

    @PostMapping("/booksByLink")
    public ResponseEntity addBooksByLink(@RequestParam String link) {
        return voidRestResponseHandler.handle(() -> bookService.addBooksFromJson(link));
    }

    @PostMapping("/booksByFile")
    public ResponseEntity addBooksByFile(@RequestBody MultipartFile file) {
        return voidRestResponseHandler.handle(() -> bookService.addBooksFromJson(file));
    }

}
