package com.senla.meb.web.handler.impl;

import com.senla.meb.errors.ResponseError;
import com.senla.meb.exception.ExceptionHandlerService;
import com.senla.meb.web.handler.api.IRestVoidResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class RestVoidResponseHandler implements IRestVoidResponseHandler {

    @Autowired
    private ExceptionHandlerService handler;

    @Override
    public ResponseEntity<?> handle(Runnable runnable) {
        HttpStatus httpStatus;
        try {
            runnable.run();
            httpStatus = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(null, httpStatus);
        } catch (Exception e) {
            final ResponseError error = handler.handle(e);
            httpStatus = HttpStatus.valueOf(error.getCode());
            return new ResponseEntity<>(error, httpStatus);
        }
    }
}
