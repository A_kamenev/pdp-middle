package com.senla.meb.web.handler.api;

import org.springframework.http.ResponseEntity;

public interface IRestVoidResponseHandler extends IGenericResponseHandler<ResponseEntity<?>, Runnable> {
}
