package com.senla.meb.web.controller;

import com.senla.meb.web.handler.api.IRestResponseHandler;
import com.senla.meb.web.handler.api.IRestVoidResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public abstract class AbstractController {

    @Autowired
    protected IRestResponseHandler restResponseHandler;

    @Autowired
    protected IRestVoidResponseHandler voidRestResponseHandler;

}
