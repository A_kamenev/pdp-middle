package com.senla.meb.web.controller;

import com.senla.meb.model.Member;
import com.senla.meb.service.api.IMemberService;
import com.senla.meb.web.dto.MemberRequestRegisterDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/members")
@Slf4j
public class MemberController extends AbstractController {

    @Autowired
    private IMemberService memberService;

    @GetMapping
    public ResponseEntity getMember() {
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = "/newRequest")
    public ResponseEntity createMembershipRequest(@RequestBody MemberRequestRegisterDto memberRequestRegisterDto) {
        return voidRestResponseHandler.handle(() -> memberService.createMembershipRequest(memberRequestRegisterDto.getTitle(), memberRequestRegisterDto.getEmail()));
    }

    @PutMapping
    public ResponseEntity editMember(@RequestBody Member member) {
        return voidRestResponseHandler.handle(() -> memberService.editMember(member));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteMember(@PathVariable Long id) {
        return voidRestResponseHandler.handle(() -> memberService.deleteMember(id));
    }



}
