package com.senla.meb.web.handler.impl;

import com.senla.meb.errors.ResponseError;
import com.senla.meb.exception.ExceptionHandlerService;
import com.senla.meb.web.handler.api.IRestResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Supplier;

@Component
public class RestResponseHandler implements IRestResponseHandler {

    @Autowired
    private ExceptionHandlerService handler;

    @Override
    public ResponseEntity<?> handle(Supplier<?> s) {
        HttpStatus httpStatus;
        try {
            Object val = Optional.ofNullable(s.get()).filter(o -> !(o instanceof Void)).orElse(null);
            if (val != null) {
                httpStatus = HttpStatus.OK;
            } else {
                httpStatus = HttpStatus.NO_CONTENT;
            }
            return new ResponseEntity<>(val, httpStatus);
        } catch (Exception e) {
            final ResponseError error = handler.handle(e);
            httpStatus = HttpStatus.valueOf(error.getCode());
            return new ResponseEntity<>(error, httpStatus);
        }
    }
}
