package com.senla.meb.web.dto;

import com.senla.meb.model.Member;
import com.senla.meb.model.RequestStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberRequestStatusDto {

    private Long idMember;

    private String status;

}