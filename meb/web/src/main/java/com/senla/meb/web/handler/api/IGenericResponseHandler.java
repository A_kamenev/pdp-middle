package com.senla.meb.web.handler.api;

@FunctionalInterface
public interface IGenericResponseHandler<T, S> {

    T handle(S supplier);

}
