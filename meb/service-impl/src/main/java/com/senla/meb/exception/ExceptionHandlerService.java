package com.senla.meb.exception;

import com.senla.meb.errors.ResponseError;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ExceptionHandlerService {

    public ResponseError handle(Exception e) {
        int status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
        if (e instanceof BadDataException) {
            status = HttpStatus.SC_BAD_REQUEST;
        } else {
            log.error("{}", e.getMessage());
        }

        return new ResponseError(e.getMessage() != null ? e.getMessage() : e.getClass().getName(), status, e);
    }
}
