package com.senla.meb.service;

import com.senla.meb.dao.api.IMemberDao;
import com.senla.meb.dao.api.dictionaries.IRequestStatusDao;
import com.senla.meb.model.Member;
import com.senla.meb.model.RequestStatus;
import com.senla.meb.service.api.IAdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AdminService implements IAdminService {

    @Autowired
    private IMemberDao memberDao;

    @Autowired
    private IRequestStatusDao requestStatusDao;

    @Override
    public void changeMembershipRequestStatus(Long idMember, String status) {
        Member member = memberDao.getById(idMember);
        member.setRequestStatus(requestStatusDao.findByName(status));
        memberDao.update(member);

        log.info("Status of member with id {} is changed to {}", member.getId(), member.getRequestStatus().getName());
    }
}
