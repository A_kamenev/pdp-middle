package com.senla.meb.service.bookupload;

import com.senla.meb.enums.UploadWayEnum;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BookUploadHandlerFactory {

    public static BookUploadHandler getHandler(UploadWayEnum way) {

        BookUploadHandler handler = null;

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext();

        switch (way) {
            case FILE:
                handler = (BookUploadHandler) applicationContext.getBean("filehandler");
                break;
            case LINK:
                handler = (BookUploadHandler) applicationContext.getBean("linkhandler");
                break;
            default:
                throw new IllegalArgumentException("Incorrect upload way");
        }

        return handler;
    }

}
