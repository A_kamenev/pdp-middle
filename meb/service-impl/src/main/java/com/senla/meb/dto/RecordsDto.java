package com.senla.meb.dto;

import com.senla.meb.model.Book;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RecordsDto {

    private List<Book> records;

}
