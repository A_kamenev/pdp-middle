package com.senla.meb.service.bookupload;

import com.senla.meb.model.Book;

import java.util.List;

public interface BookUploadHandler {
    List<Book> handle();
}
