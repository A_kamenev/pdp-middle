package com.senla.meb.service;

import com.senla.meb.dao.api.IMemberDao;
import com.senla.meb.dao.api.IUserDao;
import com.senla.meb.dao.api.dictionaries.IRequestStatusDao;
import com.senla.meb.dao.api.dictionaries.IRoleDao;
import com.senla.meb.enums.RequestStatusEnum;
import com.senla.meb.enums.RoleEnum;
import com.senla.meb.model.Member;
import com.senla.meb.model.RequestStatus;
import com.senla.meb.model.User;
import com.senla.meb.service.api.IMemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
@Transactional
public class MemberService implements IMemberService {

    @Autowired
    private IMemberDao memberDao;

    @Autowired
    private IUserDao userDao;

    @Autowired
    private IRequestStatusDao requestStatusDao;

    @Autowired
    private IRoleDao roleDao;

    @Override
    public void createMembershipRequest(String title, String email) {
        log.info("Creating new member with title \"{}\" and email \"{}\"", title, email);
        Member member = new Member();
        member.setTitle(title);
        member.setJoinDate(LocalDateTime.now());
        member.setRequestStatus(requestStatusDao.findByName(RequestStatusEnum.CONSIDERATION));

        memberDao.create(member);

        log.debug("New member record with title \"{}\" created", title);

        User user = new User();
        user.setEmail(email);
        user.setMember(member);
        user.setRole(roleDao.findByName(RoleEnum.ROLE_MEMBER));
        userDao.create(user);

        log.debug("New user record with email \"{}\" for member with title \"{}\" created", email, title);
        log.info("New member with title \"{}\" and email \"{}\" created", title, email);

    }

    @Override
    public Member getMemberById(Long id) {
        return memberDao.getById(id);
    }

    @Override
    public List<Member> getAllMembers() {
        return memberDao.getAll();
    }

    @Override
    public void editMember(Member member) {

        memberDao.update(member);

        log.info("Edit member with id {}", member.getId());

    }

    @Override
    public void deleteMember(Long id) {
        memberDao.deleteById(id);

        log.info("Edit member with id {}", id);
    }


}
