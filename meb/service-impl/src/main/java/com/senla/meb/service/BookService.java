package com.senla.meb.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.senla.meb.dto.RecordsDto;
import com.senla.meb.exception.BadDataException;
import com.senla.meb.model.Book;
import com.senla.meb.dao.api.IBookDao;
import com.senla.meb.service.api.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@Transactional
public class BookService implements IBookService {

    @Autowired
    private IBookDao bookDao;

    @Override
    public List<Book> getAllBooks() {
        return bookDao.getAll();
    }

    @Override
    public Book getBookById(Long id) {
        return bookDao.getById(id);
    }

    @Override
    public void addBook(Book book) {
        bookDao.create(book);
    }

    @Override
    public void updateBook(Book book) {
        bookDao.update(book);
    }

    @Override
    public void deleteBook(Long id) {
        bookDao.deleteById(id);
    }

    @Override
    public void addBooks(List<Book> books) {

    }

    @Override
    public void addBooksFromJson(String link) {

    }

    @Override
    public void addBooksFromJson(MultipartFile file) {
        try {
            byte[] bytes = file.getBytes();
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                    false);
            RecordsDto records = mapper.readValue(bytes, RecordsDto.class);
            if (records == null) {
                throw new BadDataException("No records found");
            }

            addBooks(records.getRecords());
        } catch (IOException e) {
            throw new BadDataException("Your file is wrong: " + e.getMessage());
        }
    }


}
