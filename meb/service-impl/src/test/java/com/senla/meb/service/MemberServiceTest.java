package com.senla.meb.service;

import com.senla.meb.dao.api.IMemberDao;
import com.senla.meb.model.Member;
import com.senla.meb.service.api.IMemberService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class MemberServiceTest {

    @InjectMocks
    private IMemberService memberService;

    @Mock
    private IMemberDao memberDaoMock;

    @BeforeEach
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Disabled
    void createMembershipRequestTest() {

    }

    @Test
    void getAllMembersTest() {
        /*List<Member> usersFromMock = new ArrayList<>();
        Mockito.doReturn(usersFromMock).when(memberDaoMock.getAll());

        List<Member> members = usersService.getAllUsers();

        Assert.assertEquals(users,usersFromMock);
        Mockito.when();*/
    }
}