package com.senla.meb.enums;

public enum RequestStatusEnum {
    APPROVED,
    CONSIDERATION,
    REJECTED
}
