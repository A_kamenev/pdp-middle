package com.senla.meb.errors;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;

@Getter
@Setter
public class ResponseError {

    private String trace;
    private String message;
    private Exception exception;
    private int code;

    public ResponseError(String message, int code, Exception exception) {
        this.message = message;
        this.exception = exception;
        if (exception != null) {
            this.trace = Arrays.toString(exception.getStackTrace());
        }
        this.code = code;
    }
}

