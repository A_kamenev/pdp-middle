package com.senla.meb.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "upload_way")
@Getter
@Setter
@Immutable
public class UploadWay {

    @Id
    @Column(name = "id_upload_way", nullable = false, unique = true)
    private Long id;

    @Column(name = "name", nullable = false, unique = true, length = 20)
    private String name;

    @OneToMany(mappedBy = "uploadWay", fetch = FetchType.LAZY)
    private Set<UploadInfo> uploads;
}
