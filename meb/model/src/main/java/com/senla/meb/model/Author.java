package com.senla.meb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "author")
public class Author {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id_author")
    private Long id;

    @Column(name = "family", nullable = false, length = 45)
    private String family;

    @Column(name = "name", nullable = false, length = 45)
    private String name;

    @Column(name = "patronymic", length = 45)
    private String patronymic;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "book_author",
            joinColumns = {@JoinColumn(name = "id_author")},
            inverseJoinColumns = {@JoinColumn(name = "id_book")}
    )
    private Set<Book> books;
}
