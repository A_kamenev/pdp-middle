package com.senla.meb.service.api;

import com.senla.meb.model.Book;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IBookService {

    public List<Book> getAllBooks();

    public Book getBookById(Long id);

    public void addBook(Book book);

    public void updateBook(Book book);

    public void deleteBook(Long id);

    public void addBooks(List<Book> books);

    public void addBooksFromJson(String link);

    public void addBooksFromJson(MultipartFile file);
}
