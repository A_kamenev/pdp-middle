package com.senla.meb.exception;

public class BadDataException extends RuntimeException {

    public BadDataException(String s) {
        super(s);
    }

}
