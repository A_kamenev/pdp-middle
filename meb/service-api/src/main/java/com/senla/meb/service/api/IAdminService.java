package com.senla.meb.service.api;

import com.senla.meb.model.Member;
import com.senla.meb.model.RequestStatus;

public interface IAdminService {

    void changeMembershipRequestStatus(Long idMember, String status);

}
