package com.senla.meb.service.api;

import com.senla.meb.model.Member;

import java.util.List;

public interface IMemberService {

    public void createMembershipRequest(String title, String email);

    public Member getMemberById(Long id);

    public List<Member> getAllMembers();

    public void editMember(Member member);

    public void deleteMember(Long id);

}
