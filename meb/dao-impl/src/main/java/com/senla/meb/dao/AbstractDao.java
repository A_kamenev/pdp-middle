package com.senla.meb.dao;

import com.senla.meb.dao.api.IGenericDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public class AbstractDao<T, PK extends Serializable> implements IGenericDao<T,PK> {

    private Class<T> clazz;

    @PersistenceContext
    protected EntityManager entityManager;

    public AbstractDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass()
                .getGenericSuperclass();
        this.clazz = (Class<T>) genericSuperclass
                .getActualTypeArguments()[0];
    }

    public final void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public void create(T obj) {
        entityManager.persist(obj);
    }

    @Override
    public void update(T obj) {
        entityManager.merge(obj);
    }

    @Override
    public T getById(PK id) {
        return entityManager.find(clazz, id);
    }

    @Override
    public List<T> getAll() {
        return entityManager.createQuery("from " + clazz.getName())
                .getResultList();
    }

    @Override
    public void delete(T obj) {
        entityManager.remove(obj);
    }

    @Override
    public void deleteById(PK id) {
        T obj = this.getById(id);
        if (obj != null) {
            this.delete(obj);
        }
    }


}
