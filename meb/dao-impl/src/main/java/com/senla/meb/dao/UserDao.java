package com.senla.meb.dao;

import com.senla.meb.model.User;
import com.senla.meb.dao.api.IUserDao;
import com.senla.meb.model.User_;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class UserDao extends AbstractDao<User, Long> implements IUserDao {

    @Override
    public User getUserByEmail(String email) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = builder.createQuery(User.class);
        Root<User> userRoot = query.from(User.class);
        query.select(userRoot).where(builder.equal(userRoot.get(User_.email), email));

        return entityManager.createQuery(query).getSingleResult();
    }
}
