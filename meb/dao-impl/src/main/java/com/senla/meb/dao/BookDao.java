package com.senla.meb.dao;

import com.senla.meb.model.Book;
import com.senla.meb.dao.api.IBookDao;
import org.springframework.stereotype.Repository;

@Repository
public class BookDao extends AbstractDao<Book, Long> implements IBookDao {

}
