package com.senla.meb.dao;

import com.senla.meb.dao.api.IMemberDao;
import com.senla.meb.model.Member;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDao extends AbstractDao<Member, Long> implements IMemberDao {

}
