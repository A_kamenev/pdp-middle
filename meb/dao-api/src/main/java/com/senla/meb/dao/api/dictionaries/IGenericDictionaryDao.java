package com.senla.meb.dao.api.dictionaries;

import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface IGenericDictionaryDao <T, PK extends Serializable> {

    public default T findByName(Enum enumName) {
        return findByName(enumName.toString());
    }

    T findByName(String name);

}
