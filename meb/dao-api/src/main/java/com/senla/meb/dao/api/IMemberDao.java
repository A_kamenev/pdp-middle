package com.senla.meb.dao.api;

import com.senla.meb.model.Member;
import com.senla.meb.model.RequestStatus;
import org.springframework.stereotype.Repository;

public interface IMemberDao extends IGenericDao<Member, Long> {

}
