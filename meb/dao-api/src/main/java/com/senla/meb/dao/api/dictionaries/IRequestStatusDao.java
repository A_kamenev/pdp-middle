package com.senla.meb.dao.api.dictionaries;

import com.senla.meb.model.RequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRequestStatusDao extends IGenericDictionaryDao<RequestStatus, Long>, JpaRepository<RequestStatus, Long> {

}
