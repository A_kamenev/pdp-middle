package com.senla.meb.dao.api.dictionaries;

import com.senla.meb.model.UploadWay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UploadWayDao extends IGenericDictionaryDao<UploadWay, Long>, JpaRepository<UploadWay, Long> {

}
