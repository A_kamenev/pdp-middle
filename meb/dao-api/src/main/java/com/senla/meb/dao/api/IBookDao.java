package com.senla.meb.dao.api;

import com.senla.meb.model.Book;

public interface IBookDao extends IGenericDao<Book, Long> {

}
