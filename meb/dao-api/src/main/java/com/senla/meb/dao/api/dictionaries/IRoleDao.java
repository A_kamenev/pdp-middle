package com.senla.meb.dao.api.dictionaries;

import com.senla.meb.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleDao extends IGenericDictionaryDao<Role, Long>, JpaRepository<Role, Long> {

}
