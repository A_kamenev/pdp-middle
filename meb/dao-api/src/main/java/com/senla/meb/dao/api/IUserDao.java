package com.senla.meb.dao.api;

import com.senla.meb.model.User;

public interface IUserDao extends IGenericDao<User, Long> {
    public User getUserByEmail(String email);
}
