package com.senla.meb.dao.api;

import java.io.Serializable;
import java.util.List;

public interface IGenericDao <T, PK extends Serializable> {

    void create(T obj);

    void update(T obj);

    T getById(PK id);

    List<T> getAll();

    void delete(T obj);

    void deleteById(PK id);
}
